################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (11.3.rel1)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../tiny/arch/arm/arm-v8m/cortex-m33/gcc/port_c.c 

S_UPPER_SRCS += \
../tiny/arch/arm/arm-v8m/cortex-m33/gcc/port_s.S 

OBJS += \
./tiny/arch/arm/arm-v8m/cortex-m33/gcc/port_c.o \
./tiny/arch/arm/arm-v8m/cortex-m33/gcc/port_s.o 

S_UPPER_DEPS += \
./tiny/arch/arm/arm-v8m/cortex-m33/gcc/port_s.d 

C_DEPS += \
./tiny/arch/arm/arm-v8m/cortex-m33/gcc/port_c.d 


# Each subdirectory must supply rules for building sources it contributes
tiny/arch/arm/arm-v8m/cortex-m33/gcc/%.o tiny/arch/arm/arm-v8m/cortex-m33/gcc/%.su tiny/arch/arm/arm-v8m/cortex-m33/gcc/%.cyclo: ../tiny/arch/arm/arm-v8m/cortex-m33/gcc/%.c tiny/arch/arm/arm-v8m/cortex-m33/gcc/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m33 -std=gnu11 -g3 -DDEBUG -DUSE_FULL_LL_DRIVER -DUSE_HAL_DRIVER -DSTM32U5A5xx -c -I../Core/Inc -I../Drivers/STM32U5xx_HAL_Driver/Inc -I../Drivers/STM32U5xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32U5xx/Include -I../Drivers/CMSIS/Include -I"D:/5_Works/STM32CubeIDE/TobudOS_led/tiny/arch/arm/arm-v8m/common/include" -I"D:/5_Works/STM32CubeIDE/TobudOS_led/tiny/arch/arm/arm-v8m/cortex-m33/gcc" -I"D:/5_Works/STM32CubeIDE/TobudOS_led/tiny/kernel/core/include" -I"D:/5_Works/STM32CubeIDE/TobudOS_led/tiny/kernel/pm/include" -I"D:/5_Works/STM32CubeIDE/TobudOS_led/tiny/osal/cmsis_os" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -fcyclomatic-complexity -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv5-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
tiny/arch/arm/arm-v8m/cortex-m33/gcc/%.o: ../tiny/arch/arm/arm-v8m/cortex-m33/gcc/%.S tiny/arch/arm/arm-v8m/cortex-m33/gcc/subdir.mk
	arm-none-eabi-gcc -mcpu=cortex-m33 -g3 -DDEBUG -c -x assembler-with-cpp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv5-sp-d16 -mfloat-abi=hard -mthumb -o "$@" "$<"

clean: clean-tiny-2f-arch-2f-arm-2f-arm-2d-v8m-2f-cortex-2d-m33-2f-gcc

clean-tiny-2f-arch-2f-arm-2f-arm-2d-v8m-2f-cortex-2d-m33-2f-gcc:
	-$(RM) ./tiny/arch/arm/arm-v8m/cortex-m33/gcc/port_c.cyclo ./tiny/arch/arm/arm-v8m/cortex-m33/gcc/port_c.d ./tiny/arch/arm/arm-v8m/cortex-m33/gcc/port_c.o ./tiny/arch/arm/arm-v8m/cortex-m33/gcc/port_c.su ./tiny/arch/arm/arm-v8m/cortex-m33/gcc/port_s.d ./tiny/arch/arm/arm-v8m/cortex-m33/gcc/port_s.o

.PHONY: clean-tiny-2f-arch-2f-arm-2f-arm-2d-v8m-2f-cortex-2d-m33-2f-gcc

