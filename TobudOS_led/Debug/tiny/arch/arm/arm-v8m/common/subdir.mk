################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (11.3.rel1)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../tiny/arch/arm/arm-v8m/common/tos_cpu.c \
../tiny/arch/arm/arm-v8m/common/tos_fault.c 

OBJS += \
./tiny/arch/arm/arm-v8m/common/tos_cpu.o \
./tiny/arch/arm/arm-v8m/common/tos_fault.o 

C_DEPS += \
./tiny/arch/arm/arm-v8m/common/tos_cpu.d \
./tiny/arch/arm/arm-v8m/common/tos_fault.d 


# Each subdirectory must supply rules for building sources it contributes
tiny/arch/arm/arm-v8m/common/%.o tiny/arch/arm/arm-v8m/common/%.su tiny/arch/arm/arm-v8m/common/%.cyclo: ../tiny/arch/arm/arm-v8m/common/%.c tiny/arch/arm/arm-v8m/common/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m33 -std=gnu11 -g3 -DDEBUG -DUSE_FULL_LL_DRIVER -DUSE_HAL_DRIVER -DSTM32U5A5xx -c -I../Core/Inc -I../Drivers/STM32U5xx_HAL_Driver/Inc -I../Drivers/STM32U5xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32U5xx/Include -I../Drivers/CMSIS/Include -I"D:/5_Works/STM32CubeIDE/TobudOS_led/tiny/arch/arm/arm-v8m/common/include" -I"D:/5_Works/STM32CubeIDE/TobudOS_led/tiny/arch/arm/arm-v8m/cortex-m33/gcc" -I"D:/5_Works/STM32CubeIDE/TobudOS_led/tiny/kernel/core/include" -I"D:/5_Works/STM32CubeIDE/TobudOS_led/tiny/kernel/pm/include" -I"D:/5_Works/STM32CubeIDE/TobudOS_led/tiny/osal/cmsis_os" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -fcyclomatic-complexity -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv5-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-tiny-2f-arch-2f-arm-2f-arm-2d-v8m-2f-common

clean-tiny-2f-arch-2f-arm-2f-arm-2d-v8m-2f-common:
	-$(RM) ./tiny/arch/arm/arm-v8m/common/tos_cpu.cyclo ./tiny/arch/arm/arm-v8m/common/tos_cpu.d ./tiny/arch/arm/arm-v8m/common/tos_cpu.o ./tiny/arch/arm/arm-v8m/common/tos_cpu.su ./tiny/arch/arm/arm-v8m/common/tos_fault.cyclo ./tiny/arch/arm/arm-v8m/common/tos_fault.d ./tiny/arch/arm/arm-v8m/common/tos_fault.o ./tiny/arch/arm/arm-v8m/common/tos_fault.su

.PHONY: clean-tiny-2f-arch-2f-arm-2f-arm-2d-v8m-2f-common

