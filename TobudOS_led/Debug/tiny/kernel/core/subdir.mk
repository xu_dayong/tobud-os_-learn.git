################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (11.3.rel1)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../tiny/kernel/core/tos_barrier.c \
../tiny/kernel/core/tos_binary_heap.c \
../tiny/kernel/core/tos_bitmap.c \
../tiny/kernel/core/tos_char_fifo.c \
../tiny/kernel/core/tos_completion.c \
../tiny/kernel/core/tos_countdownlatch.c \
../tiny/kernel/core/tos_event.c \
../tiny/kernel/core/tos_global.c \
../tiny/kernel/core/tos_mail_queue.c \
../tiny/kernel/core/tos_message_queue.c \
../tiny/kernel/core/tos_mmblk.c \
../tiny/kernel/core/tos_mmheap.c \
../tiny/kernel/core/tos_mutex.c \
../tiny/kernel/core/tos_pend.c \
../tiny/kernel/core/tos_priority_mail_queue.c \
../tiny/kernel/core/tos_priority_message_queue.c \
../tiny/kernel/core/tos_priority_queue.c \
../tiny/kernel/core/tos_ring_queue.c \
../tiny/kernel/core/tos_robin.c \
../tiny/kernel/core/tos_rwlock.c \
../tiny/kernel/core/tos_sched.c \
../tiny/kernel/core/tos_sem.c \
../tiny/kernel/core/tos_stopwatch.c \
../tiny/kernel/core/tos_sys.c \
../tiny/kernel/core/tos_task.c \
../tiny/kernel/core/tos_tick.c \
../tiny/kernel/core/tos_time.c \
../tiny/kernel/core/tos_timer.c 

OBJS += \
./tiny/kernel/core/tos_barrier.o \
./tiny/kernel/core/tos_binary_heap.o \
./tiny/kernel/core/tos_bitmap.o \
./tiny/kernel/core/tos_char_fifo.o \
./tiny/kernel/core/tos_completion.o \
./tiny/kernel/core/tos_countdownlatch.o \
./tiny/kernel/core/tos_event.o \
./tiny/kernel/core/tos_global.o \
./tiny/kernel/core/tos_mail_queue.o \
./tiny/kernel/core/tos_message_queue.o \
./tiny/kernel/core/tos_mmblk.o \
./tiny/kernel/core/tos_mmheap.o \
./tiny/kernel/core/tos_mutex.o \
./tiny/kernel/core/tos_pend.o \
./tiny/kernel/core/tos_priority_mail_queue.o \
./tiny/kernel/core/tos_priority_message_queue.o \
./tiny/kernel/core/tos_priority_queue.o \
./tiny/kernel/core/tos_ring_queue.o \
./tiny/kernel/core/tos_robin.o \
./tiny/kernel/core/tos_rwlock.o \
./tiny/kernel/core/tos_sched.o \
./tiny/kernel/core/tos_sem.o \
./tiny/kernel/core/tos_stopwatch.o \
./tiny/kernel/core/tos_sys.o \
./tiny/kernel/core/tos_task.o \
./tiny/kernel/core/tos_tick.o \
./tiny/kernel/core/tos_time.o \
./tiny/kernel/core/tos_timer.o 

C_DEPS += \
./tiny/kernel/core/tos_barrier.d \
./tiny/kernel/core/tos_binary_heap.d \
./tiny/kernel/core/tos_bitmap.d \
./tiny/kernel/core/tos_char_fifo.d \
./tiny/kernel/core/tos_completion.d \
./tiny/kernel/core/tos_countdownlatch.d \
./tiny/kernel/core/tos_event.d \
./tiny/kernel/core/tos_global.d \
./tiny/kernel/core/tos_mail_queue.d \
./tiny/kernel/core/tos_message_queue.d \
./tiny/kernel/core/tos_mmblk.d \
./tiny/kernel/core/tos_mmheap.d \
./tiny/kernel/core/tos_mutex.d \
./tiny/kernel/core/tos_pend.d \
./tiny/kernel/core/tos_priority_mail_queue.d \
./tiny/kernel/core/tos_priority_message_queue.d \
./tiny/kernel/core/tos_priority_queue.d \
./tiny/kernel/core/tos_ring_queue.d \
./tiny/kernel/core/tos_robin.d \
./tiny/kernel/core/tos_rwlock.d \
./tiny/kernel/core/tos_sched.d \
./tiny/kernel/core/tos_sem.d \
./tiny/kernel/core/tos_stopwatch.d \
./tiny/kernel/core/tos_sys.d \
./tiny/kernel/core/tos_task.d \
./tiny/kernel/core/tos_tick.d \
./tiny/kernel/core/tos_time.d \
./tiny/kernel/core/tos_timer.d 


# Each subdirectory must supply rules for building sources it contributes
tiny/kernel/core/%.o tiny/kernel/core/%.su tiny/kernel/core/%.cyclo: ../tiny/kernel/core/%.c tiny/kernel/core/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m33 -std=gnu11 -g3 -DDEBUG -DUSE_FULL_LL_DRIVER -DUSE_HAL_DRIVER -DSTM32U5A5xx -c -I../Core/Inc -I../Drivers/STM32U5xx_HAL_Driver/Inc -I../Drivers/STM32U5xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32U5xx/Include -I../Drivers/CMSIS/Include -I"D:/5_Works/STM32CubeIDE/TobudOS_led/tiny/arch/arm/arm-v8m/common/include" -I"D:/5_Works/STM32CubeIDE/TobudOS_led/tiny/arch/arm/arm-v8m/cortex-m33/gcc" -I"D:/5_Works/STM32CubeIDE/TobudOS_led/tiny/kernel/core/include" -I"D:/5_Works/STM32CubeIDE/TobudOS_led/tiny/kernel/pm/include" -I"D:/5_Works/STM32CubeIDE/TobudOS_led/tiny/osal/cmsis_os" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -fcyclomatic-complexity -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv5-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-tiny-2f-kernel-2f-core

clean-tiny-2f-kernel-2f-core:
	-$(RM) ./tiny/kernel/core/tos_barrier.cyclo ./tiny/kernel/core/tos_barrier.d ./tiny/kernel/core/tos_barrier.o ./tiny/kernel/core/tos_barrier.su ./tiny/kernel/core/tos_binary_heap.cyclo ./tiny/kernel/core/tos_binary_heap.d ./tiny/kernel/core/tos_binary_heap.o ./tiny/kernel/core/tos_binary_heap.su ./tiny/kernel/core/tos_bitmap.cyclo ./tiny/kernel/core/tos_bitmap.d ./tiny/kernel/core/tos_bitmap.o ./tiny/kernel/core/tos_bitmap.su ./tiny/kernel/core/tos_char_fifo.cyclo ./tiny/kernel/core/tos_char_fifo.d ./tiny/kernel/core/tos_char_fifo.o ./tiny/kernel/core/tos_char_fifo.su ./tiny/kernel/core/tos_completion.cyclo ./tiny/kernel/core/tos_completion.d ./tiny/kernel/core/tos_completion.o ./tiny/kernel/core/tos_completion.su ./tiny/kernel/core/tos_countdownlatch.cyclo ./tiny/kernel/core/tos_countdownlatch.d ./tiny/kernel/core/tos_countdownlatch.o ./tiny/kernel/core/tos_countdownlatch.su ./tiny/kernel/core/tos_event.cyclo ./tiny/kernel/core/tos_event.d ./tiny/kernel/core/tos_event.o ./tiny/kernel/core/tos_event.su ./tiny/kernel/core/tos_global.cyclo ./tiny/kernel/core/tos_global.d ./tiny/kernel/core/tos_global.o ./tiny/kernel/core/tos_global.su ./tiny/kernel/core/tos_mail_queue.cyclo ./tiny/kernel/core/tos_mail_queue.d ./tiny/kernel/core/tos_mail_queue.o ./tiny/kernel/core/tos_mail_queue.su ./tiny/kernel/core/tos_message_queue.cyclo ./tiny/kernel/core/tos_message_queue.d ./tiny/kernel/core/tos_message_queue.o ./tiny/kernel/core/tos_message_queue.su ./tiny/kernel/core/tos_mmblk.cyclo ./tiny/kernel/core/tos_mmblk.d ./tiny/kernel/core/tos_mmblk.o ./tiny/kernel/core/tos_mmblk.su ./tiny/kernel/core/tos_mmheap.cyclo ./tiny/kernel/core/tos_mmheap.d ./tiny/kernel/core/tos_mmheap.o ./tiny/kernel/core/tos_mmheap.su ./tiny/kernel/core/tos_mutex.cyclo ./tiny/kernel/core/tos_mutex.d ./tiny/kernel/core/tos_mutex.o ./tiny/kernel/core/tos_mutex.su ./tiny/kernel/core/tos_pend.cyclo ./tiny/kernel/core/tos_pend.d ./tiny/kernel/core/tos_pend.o ./tiny/kernel/core/tos_pend.su ./tiny/kernel/core/tos_priority_mail_queue.cyclo ./tiny/kernel/core/tos_priority_mail_queue.d ./tiny/kernel/core/tos_priority_mail_queue.o ./tiny/kernel/core/tos_priority_mail_queue.su ./tiny/kernel/core/tos_priority_message_queue.cyclo ./tiny/kernel/core/tos_priority_message_queue.d ./tiny/kernel/core/tos_priority_message_queue.o ./tiny/kernel/core/tos_priority_message_queue.su ./tiny/kernel/core/tos_priority_queue.cyclo ./tiny/kernel/core/tos_priority_queue.d ./tiny/kernel/core/tos_priority_queue.o ./tiny/kernel/core/tos_priority_queue.su ./tiny/kernel/core/tos_ring_queue.cyclo ./tiny/kernel/core/tos_ring_queue.d ./tiny/kernel/core/tos_ring_queue.o ./tiny/kernel/core/tos_ring_queue.su ./tiny/kernel/core/tos_robin.cyclo ./tiny/kernel/core/tos_robin.d ./tiny/kernel/core/tos_robin.o ./tiny/kernel/core/tos_robin.su ./tiny/kernel/core/tos_rwlock.cyclo ./tiny/kernel/core/tos_rwlock.d ./tiny/kernel/core/tos_rwlock.o ./tiny/kernel/core/tos_rwlock.su ./tiny/kernel/core/tos_sched.cyclo ./tiny/kernel/core/tos_sched.d ./tiny/kernel/core/tos_sched.o ./tiny/kernel/core/tos_sched.su ./tiny/kernel/core/tos_sem.cyclo ./tiny/kernel/core/tos_sem.d ./tiny/kernel/core/tos_sem.o ./tiny/kernel/core/tos_sem.su ./tiny/kernel/core/tos_stopwatch.cyclo ./tiny/kernel/core/tos_stopwatch.d ./tiny/kernel/core/tos_stopwatch.o ./tiny/kernel/core/tos_stopwatch.su ./tiny/kernel/core/tos_sys.cyclo ./tiny/kernel/core/tos_sys.d ./tiny/kernel/core/tos_sys.o ./tiny/kernel/core/tos_sys.su ./tiny/kernel/core/tos_task.cyclo ./tiny/kernel/core/tos_task.d ./tiny/kernel/core/tos_task.o ./tiny/kernel/core/tos_task.su ./tiny/kernel/core/tos_tick.cyclo ./tiny/kernel/core/tos_tick.d ./tiny/kernel/core/tos_tick.o ./tiny/kernel/core/tos_tick.su ./tiny/kernel/core/tos_time.cyclo ./tiny/kernel/core/tos_time.d ./tiny/kernel/core/tos_time.o ./tiny/kernel/core/tos_time.su ./tiny/kernel/core/tos_timer.cyclo ./tiny/kernel/core/tos_timer.d ./tiny/kernel/core/tos_timer.o ./tiny/kernel/core/tos_timer.su

.PHONY: clean-tiny-2f-kernel-2f-core

