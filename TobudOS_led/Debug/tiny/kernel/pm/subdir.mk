################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (11.3.rel1)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../tiny/kernel/pm/tos_pm.c \
../tiny/kernel/pm/tos_tickless.c 

OBJS += \
./tiny/kernel/pm/tos_pm.o \
./tiny/kernel/pm/tos_tickless.o 

C_DEPS += \
./tiny/kernel/pm/tos_pm.d \
./tiny/kernel/pm/tos_tickless.d 


# Each subdirectory must supply rules for building sources it contributes
tiny/kernel/pm/%.o tiny/kernel/pm/%.su tiny/kernel/pm/%.cyclo: ../tiny/kernel/pm/%.c tiny/kernel/pm/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m33 -std=gnu11 -g3 -DDEBUG -DUSE_FULL_LL_DRIVER -DUSE_HAL_DRIVER -DSTM32U5A5xx -c -I../Core/Inc -I../Drivers/STM32U5xx_HAL_Driver/Inc -I../Drivers/STM32U5xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32U5xx/Include -I../Drivers/CMSIS/Include -I"D:/5_Works/STM32CubeIDE/TobudOS_led/tiny/arch/arm/arm-v8m/common/include" -I"D:/5_Works/STM32CubeIDE/TobudOS_led/tiny/arch/arm/arm-v8m/cortex-m33/gcc" -I"D:/5_Works/STM32CubeIDE/TobudOS_led/tiny/kernel/core/include" -I"D:/5_Works/STM32CubeIDE/TobudOS_led/tiny/kernel/pm/include" -I"D:/5_Works/STM32CubeIDE/TobudOS_led/tiny/osal/cmsis_os" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -fcyclomatic-complexity -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv5-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-tiny-2f-kernel-2f-pm

clean-tiny-2f-kernel-2f-pm:
	-$(RM) ./tiny/kernel/pm/tos_pm.cyclo ./tiny/kernel/pm/tos_pm.d ./tiny/kernel/pm/tos_pm.o ./tiny/kernel/pm/tos_pm.su ./tiny/kernel/pm/tos_tickless.cyclo ./tiny/kernel/pm/tos_tickless.d ./tiny/kernel/pm/tos_tickless.o ./tiny/kernel/pm/tos_tickless.su

.PHONY: clean-tiny-2f-kernel-2f-pm

